#Script is intended to be stored in C:\attachment-print
#If you prefer another directory, feel free to modify the code below :)
#to execute task silently in background, use C:\attachment-print-rivalta2\task.vbs


$Username = ""
$Password = ""
$printer = ""


$DEFAULTPRINTER = (Get-CimInstance -ClassName CIM_Printer | WHERE {$_.Default -eq $True}[0])

if (!(Test-Path 'attachments' -PathType Container)) {
  New-Item -ItemType Directory -Force -Path 'attachments'
}
if (!(Test-Path 'printed-attachments' -PathType Container)) {
  New-Item -ItemType Directory -Force -Path 'printed-attachments'
}

$ImapXpath = [string](Get-Location) + "\ImapX.dll"
$Attachmentpath = [string](Get-Location) + "\attachments\"
$Attachmentpathfile = [string](Get-Location) + "\attachments\*"
$Attachmentpathfiles = [string](Get-Location) + "\attachments\*.*"
$PrintedAttachmentpath = [string](Get-Location) + '\printed-attachments'


$Attachmentpath

#Load ImapX.dll (IMAP client, https://github.com/azanov/imapx )
[Reflection.Assembly]::LoadFile($ImapXpath)


#Clean downloaded attachment folder, in case some garbage is left there.
Remove-Item $Attachmentpathfiles -Force


$client = New-Object ImapX.ImapClient
$client.Behavior.MessageFetchMode = "Full"
$client.Host = "imaps.aruba.it"
$client.Port = 993
$client.UseSsl = $true
$client.SslProtocol = [Net.SecurityProtocolType]::Tls12
$client.Connect()
$client.Login($Username, $Password)


#select inbox folder
$res = $client.folders| where { $_.path -eq 'Inbox' }

$trash = $res.subFolders| where { $_.path -eq 'INBOX.Trash' }

# fetch last 100 messages
while(1){
$numberOfMessagesLimit = 100
$messages = $res.search("ALL", $client.Behavior.MessageFetchMode, $numberOfMessagesLimit)

# Display the messages in a formatted table
# $messages | ft *

#download attachements from each unread message ( including inline attachements )
foreach($m in $messages) {
  if($m.Seen) {
  } else {
    if($m.Subject -eq 'New Customer'){
      write-host($m.Subject)
      foreach($r in $m.Attachments) {
          $r.Download()
          $r.Save([string]($Attachmentpath))
      }
      $m.Flags.Add("\Seen")
      $m.MoveTo($trash)
    }
  }
#mark messages as read
}
#wait 20 seconds, in some cases files appear to late.
timeout 10


#delete empty attechements and files smaller than 1kb
Get-ChildItem $Attachmentpath -Filter *.stat -recurse |?{$_.PSIsContainer -eq $false -and $_.length -lt 1000}|?{Remove-Item $_.fullname -WhatIf}
Get-ChildItem -Path $Attachmentpath -Recurse -Force | Where-Object { $_.PSIsContainer -eq $false -and $_.Length -eq 0 } | remove-item

#delete emoticons
Remove-Item .\attachments\*Emoticon* -Force
Remove-Item .\attachments\*emoticon* -Force
Remove-Item .\attachments\*smiley* -Force

#Print PDF files
$FilesToPrint = Get-ChildItem $Attachmentpathfile -Recurse -Include *.pdf,*.PDF
# $FilesToPrint
foreach($File in $FilesToPrint) {
    Write-Host $File.FullName
    $PRINTERTMP = (Get-CimInstance -ClassName CIM_Printer | WHERE {$_.NAme -eq $printer}[0])
    $PRINTERTMP | Invoke-CimMethod -MethodName SetDefaultPrinter | Out-Null

    Start-Process -WindowStyle Hidden -FilePath $File.Name -WorkingDirectory $Attachmentpath -Verb print | %{ Start-Sleep 15;$_ } | Stop-Process
    $DEFAULTPRINTER | Invoke-CimMethod -MethodName SetDefaultPrinter | Out-Null
    Start-Sleep -Seconds 10
}

#move printed files to printed-attachments folder
Get-ChildItem $Attachmentpathfile -Include *.pdf,*.PDF | Move-Item -Force -Destination $PrintedAttachmentpath

#delete garbage
Remove-Item $Attachmentpathfiles -Force

#delete printed-attachments older than 60 days
$limit = (Get-Date).AddDays(-60)
Get-ChildItem -Path $PrintedAttachmentpath -Recurse -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit } | Remove-Item -Force
}
